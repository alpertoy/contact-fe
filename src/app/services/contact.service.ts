import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  sendEmail(contactFormDTO: any): Observable<any> {
    return this.http.post("http://localhost:8080/api/contact/sendEmail", contactFormDTO);
  }
}
