import { ContactService } from './../../services/contact.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit{

  @ViewChild('formInput') formInput: ElementRef;

  infoMessage: string = '';

  validatingForm: FormGroup;

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    this.validatingForm = new FormGroup({
      contactFormFullName: new FormControl('', Validators.required),
      contactFormEmail: new FormControl('', [Validators.email, Validators.required]),
      contactFormSubject: new FormControl('', Validators.required),
      contactFormMessage: new FormControl('', Validators.required),
    });
  }

  get contactFormFullName() {
    return this.validatingForm.get('contactFormFullName');
  }

  get contactFormEmail() {
    return this.validatingForm.get('contactFormEmail');
  }

  get contactFormSubject() {
    return this.validatingForm.get('contactFormSubject');
  }

  get contactFormMessage() {
    return this.validatingForm.get('contactFormMessage');
  }

  sendEmail(): void {
    let contactFormDTO = {
      name: this.contactFormFullName.value,
      email: this.contactFormEmail.value,
      subject: this.contactFormSubject.value,
      message: this.contactFormMessage.value
    }

    this.contactService.sendEmail(contactFormDTO).subscribe(data => {
      this.infoMessage = 'Your message has been received, We will contact you soon.';
      setTimeout(() => {this.infoMessage = '';}, 6000);
      this.validatingForm.reset();
    }, error => {
      this.infoMessage = 'Error sending email';
      setTimeout(() => {this.infoMessage = '';}, 6000);
      console.log('Error ' + JSON.stringify(error));
    });
  }

}
